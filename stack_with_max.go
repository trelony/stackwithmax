package stackwithmax

import "fmt"

type StackWithMax struct {
	head *Element
}

func NewStack() *StackWithMax {
	return &StackWithMax{
	}
}

func (s *StackWithMax) Push(val int) {
	s.head = NewElement(s.head, val)
}

func (s *StackWithMax) Pop() (int, error) {
	if s.head == nil {
		return 0, fmt.Errorf("No elements in stack")
	}

	val := s.head.getValue()
	s.head = s.head.getNext()
	return val, nil

}

func (s *StackWithMax) GetMax() (int, error) {
	if s.head == nil {
		return 0, fmt.Errorf("No elements in stack")
	}

	return s.head.getCurrentMax(), nil
}

type Element struct {
	value      int
	currentMax int
	prev       *Element
}

func (e *Element) getCurrentMax() (int) {
	return e.currentMax
}

func (e *Element) getValue() int {
	return e.value
}

func (e *Element) getNext() *Element {
	return e.prev
}

func NewElement(prev *Element, val int) *Element {
	if prev == nil {
		return &Element{
			value:val,
			currentMax:val,
		}
	}

	newElem := &Element{
		value:      val,
		currentMax: getmaxval(val, prev.getCurrentMax()),
		prev:       prev,
	}

	return newElem
}

func getmaxval(first, second int) int {
	if first >= second {
		return first
	}

	return second
}